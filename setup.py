from setuptools import setup, find_packages

setup(
    name="addvolt_ml_utils",
    version="0.0.24",
    author='Leonel Araujo',
    author_email='leonel.araujo@addvolt.com',
    license="BSD",
    packages=find_packages(),
    install_requires=[
        'pandas==1.1.1',
        'Shapely==1.7.0',
        'mgrs==1.3.6'
    ]
)
