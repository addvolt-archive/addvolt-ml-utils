import unittest
import pandas as pd
from addvolt_ml_utils.preprocessing.tail_lift import add_tail_lift_columns


class AddTailLiftFlagsTest(unittest.TestCase):
    def setUp(self):
        data = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
        )
        self.df = pd.DataFrame(data=data)
        pass

    def tearDown(self):
        pass

    def test_always_on(self):
        df = self.df.copy()
        df['timer_digital_3'] = 1
        result = add_tail_lift_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['tail_lift_is_on'].value_counts()[True], result.shape[0])  # Every value is True
        self.assertEqual(result['tail_lift_startup'].value_counts()[False], result.shape[0])  # Every value is False
        self.assertEqual(sum(result['tail_lift_operating_timer']), df.shape[0])

    def test_always_off(self):
        df = self.df.copy()
        df['timer_digital_3'] = 0
        result = add_tail_lift_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['tail_lift_is_on'].value_counts()[False], result.shape[0])  # Every value is True
        self.assertEqual(result['tail_lift_startup'].value_counts()[False], result.shape[0])  # Every value is False
        self.assertEqual(sum(result['tail_lift_operating_timer']), 0)

    def test_check_parameter_field(self):
        df = self.df.copy()
        df['new_field'] = 1
        result = add_tail_lift_columns(df, field='new_field')
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['tail_lift_is_on'].value_counts()[True], result.shape[0])  # Every value is True
        self.assertEqual(result['tail_lift_startup'].value_counts()[False], result.shape[0])  # Every value is False
        self.assertEqual(sum(result['tail_lift_operating_timer']), df.shape[0])

    def test_only_every_30s_a(self):
        df = self.df.copy()
        df.loc[9, 'timer_digital_3'] = 5
        df.loc[39, 'timer_digital_3'] = 6
        df.loc[69, 'timer_digital_3'] = 0

        result = add_tail_lift_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['tail_lift_is_on'].value_counts()[True], 40)
        self.assertEqual(result['tail_lift_is_on'].value_counts()[False], 30)
        self.assertEqual(result['tail_lift_operating_timer'].sum(), 11)
        self.assertEqual(result['tail_lift_startup'].value_counts()[False], 100)  # No startup detected since the tail lift was already ON

if __name__ == '__main__':
    unittest.main()
