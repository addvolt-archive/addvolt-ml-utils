import unittest
import pandas as pd
import numpy as np
from addvolt_ml_utils.preprocessing import maps_data_cleaning


class MapsDataCleaningTest(unittest.TestCase):
    def setUp(self):
        data = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
            latitude=np.arange(40, 41, 0.01),
            longitude=np.arange(-9, -8, 0.01),
            rpm=np.random.randint(0, 3000, 100)
        )
        self.df = pd.DataFrame(data=data)

    def tearDown(self):
        pass

    def test_maps_data_cleaning_keeps_columns(self):
        df = maps_data_cleaning(self.df)

        columns = df.columns.tolist()
        self.assertTrue('registered_at' in columns)
        self.assertTrue('latitude' in columns)
        self.assertTrue('longitude' in columns)
        self.assertTrue('rpm' in columns)

    def test_maps_data_cleaning_flags_rows_with_invalid_gps(self):
        # Inserting absurd/invalid gps values
        df_with_invalid_gps = self.df.copy()
        df_with_invalid_gps.loc[2, 'latitude'] = -300
        df_with_invalid_gps.loc[2, 'longitude'] = -300
        df_with_invalid_gps.loc[7, 'latitude'] = -300
        df_with_invalid_gps.loc[7, 'longitude'] = -300

        df_filtered = maps_data_cleaning(df_with_invalid_gps)

        columns = df_with_invalid_gps.columns.tolist()
        self.assertTrue('latitude' in columns)
        self.assertTrue('longitude' in columns)
        self.assertTrue(df_filtered.shape[0] == self.df.shape[0] - 4)

    def test_maps_data_cleaning_removes_rows_with_invalid_gps_2(self):
        # Inserting absurd/invalid gps values
        df_with_invalid_gps = self.df.copy()
        df_with_invalid_gps.loc[2, 'latitude'] = -300
        df_with_invalid_gps.loc[2, 'longitude'] = -300
        df_with_invalid_gps.loc[5, 'latitude'] = 0
        df_with_invalid_gps.loc[5, 'longitude'] = 0
        df_with_invalid_gps.loc[6, 'latitude'] = -300
        df_with_invalid_gps.loc[6, 'longitude'] = -300

        df_filtered = maps_data_cleaning(df_with_invalid_gps)

        columns = df_with_invalid_gps.columns.tolist()
        self.assertTrue('latitude' in columns)
        self.assertTrue('longitude' in columns)
        self.assertTrue(df_filtered.shape[0] == self.df.shape[0] - 5)

    def test_maps_data_cleaning_removes_rows_with_invalid_gps_3(self):
        # Inserting absurd/invalid gps values
        df_with_invalid_gps = self.df.copy()
        df_with_invalid_gps.loc[2, 'latitude'] = -300
        df_with_invalid_gps.loc[2, 'longitude'] = -300
        df_with_invalid_gps.loc[3, 'latitude'] = 0
        df_with_invalid_gps.loc[3, 'longitude'] = 0
        df_with_invalid_gps.loc[4, 'latitude'] = -300
        df_with_invalid_gps.loc[4, 'longitude'] = -300
        df_with_invalid_gps.loc[6, 'latitude'] = df_with_invalid_gps.loc[5, 'latitude'] + 0.5
        df_with_invalid_gps.loc[6, 'longitude'] = df_with_invalid_gps.loc[5, 'longitude'] + 0.5

        df_filtered = maps_data_cleaning(df_with_invalid_gps)

        columns = df_with_invalid_gps.columns.tolist()
        self.assertTrue('latitude' in columns)
        self.assertTrue('longitude' in columns)
        self.assertTrue(df_filtered.shape[0] == self.df.shape[0] - 6)

if __name__ == '__main__':
    unittest.main()
