import unittest
import datetime
import pandas as pd
from addvolt_ml_utils.preprocessing.system import add_system_columns


class AddSystemFlagsTest(unittest.TestCase):
    def setUp(self):
        data = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
        )
        self.df = pd.DataFrame(data=data)
        pass

    def tearDown(self):
        pass

    def test_battery_soc(self):
        df = self.df.copy()
        df['battery_soc'] = 50

        result = add_system_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertTrue(result['system_battery_soc'].iloc[0] == 50)

    def test_system_discharging(self):
        df = self.df.copy()
        df['battery_soc'] = 50

        df.loc[1:10, 'battery_current'] = -10
        df.loc[41:60, 'battery_current'] = -10
        df.loc[61:70, 'battery_current'] = -2

        result = add_system_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertTrue(result['system_battery_soc'].iloc[0] == 50)
        self.assertTrue(result['system_is_discharging'].sum() == 30)
        self.assertTrue(result['system_is_charging'].sum() == 0)
        # self.assertTrue(result['system_battery_chg_ah'].sum() == 0)
        # self.assertAlmostEqual(result['system_battery_dchg_ah'].sum(), result[result.battery_current < -2]['battery_current'].sum() / 3600)

    def test_system_charging(self):
        df = self.df.copy()
        df['battery_soc'] = 50

        df.loc[1:10, 'battery_current'] = 10
        df.loc[41:60, 'battery_current'] = 10
        df.loc[61:70, 'battery_current'] = 1

        result = add_system_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertTrue(result['system_battery_soc'].iloc[0] == 50)
        self.assertTrue(result['system_is_discharging'].sum() == 0)
        self.assertTrue(result['system_is_charging'].sum() == 30)
        # self.assertAlmostEqual(result['system_battery_chg_ah'].sum(), result[result.battery_current > 1]['battery_current'].sum() / 3600)
        # self.assertTrue(result['system_battery_dchg_ah'].sum() == 0)

    def test_system_grid_available(self):
        df = self.df.copy()
        df['battery_soc'] = 50

        df.loc[1:19, 'grid_available'] = 0
        df.loc[20:40, 'grid_available'] = 1
        df.loc[41:100, 'grid_available'] = 15

        result = add_system_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertTrue(result['system_grid_available'].value_counts()[True], 80)
        self.assertTrue(result['system_grid_available'].value_counts()[False], 20)

    def test_system_remote_control(self):
        df = self.df.copy()
        df['battery_soc'] = 50

        df.loc[1:19, 'grid_available'] = 0
        df.loc[20:39, 'grid_available'] = 1
        df.loc[40:100, 'grid_available'] = 15

        result = add_system_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertTrue(result['system_remote_control'].value_counts()[0], 40)
        self.assertTrue(result['system_remote_control'].value_counts()[1], 60)

    def test_system_energy_balance(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 80, 1, 0.9, 1, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 80, 1, 0.9, 1, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 80, 1, 0.9, 15, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 80, 1, 0.9, 15, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 80, 1, 0, 15, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 5), 80, 1, 0, 15, 300, 2]
        ], columns=['registered_at', 'battery_soc', 'delta_t', 'generator_producing_wh', 'load_controller_current_active', 'load_controller_voltage_active', 'load_controller_state'])
        result = add_system_columns(df, labels=['regenerative_braking'])
        self.assertEqual(len(result), df.shape[0])

        # Battery is charging because Load is not requiring more energy than Braking is already providing
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_feeding_load_power'].values[0], 450)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_feeding_load_wh'].values[0], 450/3600)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_battery_chg_wh'].values[0], 0.775)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_battery_dchg_wh'].values[0], 0.0)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_charging_from_grid_wh'].values[0], 0.0)
        # Battery is discharging because the energy provided by Braking is not enough to meet the Load
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_feeding_load_power'].values[0], 6750)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_feeding_load_wh'].values[0], 6750/3600)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_battery_chg_wh'].values[0], 0)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_battery_dchg_wh'].values[0], 0.975)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_charging_from_grid_wh'].values[0], 0.0)
        # Braking has stopped producing, so all Load is being kept by the Battery
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 4), 'system_feeding_load_power'].values[0], 6750)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 4), 'system_feeding_load_wh'].values[0], 6750/3600)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 4), 'system_battery_chg_wh'].values[0], 0)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 4), 'system_battery_dchg_wh'].values[0], 1.875)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 4), 'system_charging_from_grid_wh'].values[0], 0.0)

    def test_system_energy_balance_without_braking(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 80, 1, 0.9, 1, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 80, 1, 0.9, 1, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 80, 1, 0.9, 15, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 80, 1, 0.9, 15, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 80, 1, 0, 15, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 5), 80, 1, 0, 15, 300, 2]
        ], columns=['registered_at', 'battery_soc', 'delta_t', 'generator_producing_wh', 'load_controller_current_active', 'load_controller_voltage_active', 'load_controller_state'])
        result = add_system_columns(df, labels=[])
        self.assertEqual(len(result), df.shape[0])

        # In systems without Braking, Load = Battery and "generator_producing_wh" is ignored
        # Load is requesting 450kW so that amount is being supplied by the Battery
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_feeding_load_power'].values[0], 450)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_feeding_load_wh'].values[0], 450/3600)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_battery_chg_wh'].values[0], 0.0)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_battery_dchg_wh'].values[0], 0.125)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_charging_from_grid_wh'].values[0], 0.0)
        # Load is requesting 6750kW so that amount is being supplied by the Battery
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_feeding_load_power'].values[0], 6750)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_feeding_load_wh'].values[0], 6750/3600)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_battery_chg_wh'].values[0], 0.0)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_battery_dchg_wh'].values[0], 1.875)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_charging_from_grid_wh'].values[0], 0.0)

    def test_system_charging_from_grid(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 80, 1, 15, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 80, 1, 15, 300, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 80, 1, -15, 300, 8],
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 80, 1, -15, 300, 13],
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 80, 1, -15, 300, 8],
            [datetime.datetime(2017, 1, 1, 10, 0, 5), 80, 1, 15, 300, 2]
        ], columns=['registered_at', 'battery_soc', 'delta_t', 'load_controller_current_active', 'load_controller_voltage_active', 'load_controller_state'])
        result = add_system_columns(df, labels=[])
        self.assertEqual(len(result), df.shape[0])

        # State doesn't indicate any charging
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_charging_from_grid_wh'].values[0], 0.0)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_battery_chg_wh'].values[0], 0.0)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'system_battery_dchg_wh'].values[0], 1.875)
        # Load is requesting 6750kW so that amount is being supplied by the Battery
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_charging_from_grid_wh'].values[0], 1.875)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_battery_chg_wh'].values[0], 1.875)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'system_battery_dchg_wh'].values[0], 0)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 3), 'system_charging_from_grid_wh'].values[0], 1.875)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 3), 'system_battery_chg_wh'].values[0], 1.875)
        self.assertAlmostEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 3), 'system_battery_dchg_wh'].values[0], 0.0)

    def test_dc_powered_system(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 80, 1,   5, 300, 3], # OFF 
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 80, 1,   5, 300, 3], # OFF
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 80, 1, -5, 300, 2], # ON
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 80, 1, -5, 300, 2], # ON
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 80, 1, -5, 300, 2], # ON
            [datetime.datetime(2017, 1, 1, 10, 0, 5), 80, 1,  8, 300, 8], # OFF (Charging)
        ], columns=['registered_at', 'battery_soc', 'delta_t', 'battery_current', 'battery_voltage', 'load_controller_state'])
        result = add_system_columns(df, labels=['battery', 'dc_powered'])
        self.assertEqual(len(result), df.shape[0])

        # Battery is not supplying energy, since state is not 2
        record1 = result.iloc[0]
        self.assertAlmostEqual(record1.system_battery_dchg_wh, 0)
        self.assertAlmostEqual(record1.system_feeding_load_wh, 0)
        self.assertAlmostEqual(record1.system_battery_chg_wh, 0)
        self.assertAlmostEqual(record1.system_charging_from_grid_wh, 0)
        # Battery is supplying energy, since state is 2
        record2 = result.iloc[2]
        self.assertAlmostEqual(record2.system_battery_dchg_wh, 0.416, places=2)
        self.assertAlmostEqual(record2.system_feeding_load_wh, 0.416, places=2)
        self.assertAlmostEqual(record2.system_battery_chg_wh, 0)
        self.assertAlmostEqual(record2.system_charging_from_grid_wh, 0)
        # Braking has stopped producing, so all Load is being kept by the Battery
        record3 = result.iloc[5]
        self.assertAlmostEqual(record3.system_battery_dchg_wh, 0)
        self.assertAlmostEqual(record3.system_feeding_load_wh, 0)
        self.assertAlmostEqual(record3.system_battery_chg_wh, 0.666, places=2)
        self.assertAlmostEqual(record3.system_charging_from_grid_wh, 0.666, places=2)
        # Battery charging/Discharging currents
        charging_current = df[df.battery_current > 0].battery_current.sum()
        discharging_current = df[df.battery_current < 0].battery_current.sum()
        # self.assertAlmostEqual(result['system_battery_chg_ah'].sum(), charging_current / 3600)
        # self.assertAlmostEqual(result['system_battery_dchg_ah'].sum(), discharging_current / 3600)

if __name__ == '__main__':
    unittest.main()
