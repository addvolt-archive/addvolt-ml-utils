import unittest
import pandas as pd
from addvolt_ml_utils.preprocessing.operation import prune_vehicle_status_column


class AddOperationFlagsTest(unittest.TestCase):
    def setUp(self):
        data = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
            vehicle_status='stopped',
        )
        self.df = pd.DataFrame(data=data)

    def tearDown(self):
        pass

    def test_pruning_keeps_shape(self):
        pruned_signal = prune_vehicle_status_column(self.df)
        self.assertEqual(len(pruned_signal), self.df.shape[0])

    def test_pruning_valid_1(self):
        df_prune = self.df.copy()
        df_prune.loc[1, 'vehicle_status'] = 'moving'
        df_prune.loc[5, 'vehicle_status'] = 'moving'
        pruned_signal = prune_vehicle_status_column(df_prune)
        self.assertTrue(len(pruned_signal) == df_prune.shape[0])

    def test_pruning_valid_2(self):
        df_prune = self.df.copy()
        df_prune.loc[1:5, 'vehicle_status'] = 'moving'
        pruned_signal = prune_vehicle_status_column(df_prune)
        self.assertTrue(len(pruned_signal) == df_prune.shape[0])
        self.assertTrue(pruned_signal.value_counts()['stopped'] == df_prune.shape[0])
        with self.assertRaises(KeyError):
            pruned_signal.value_counts()['moving']

    def test_pruning_valid_3(self):
        df_prune = self.df.copy()
        df_prune.loc[1:50, 'vehicle_status'] = 'moving'
        pruned_signal = prune_vehicle_status_column(df_prune, delta_t_threshold=30)
        self.assertTrue(len(pruned_signal) == df_prune.shape[0])
        self.assertTrue(pruned_signal.value_counts()['stopped'] == 50)
        self.assertTrue(pruned_signal.value_counts()['moving'] == 50)


if __name__ == '__main__':
    unittest.main()
