import unittest
import datetime
import pandas as pd
from addvolt_ml_utils.preprocessing.braking import add_braking_flags


class AddBrakingFlagsTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_default(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 1, 70, 1, 180, 400, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 1, 70, 1, 180, 400, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 1, 70, 15, 180, 400, 4],
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 1, 70, 15, 180, 400, 4],
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 1, 70, 1, 180, 400, 0]
        ], columns=['registered_at', 'delta_t', 'speed_can', 'motor_controller_current_ac', 'motor_controller_voltage_ac', 'motor_controller_angular_speed', 'motor_controller_state'])
        result = add_braking_flags(df, labels=['can'])
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'generator_power'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'generator_producing_wh'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'generator_producing_energy'].values[0], False)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'generator_power'].values[0], 180 * 15 * 1.5)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'generator_producing_wh'].values[0], 180 * 15 * 1.5 / 3600)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'generator_producing_energy'].values[0], True)
        self.assertAlmostEquals(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'generator_rpm'].values[0], 3819.71, places=1)

    def test_default_missing_speed(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 1, None, 1, 180, 400, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 1, None, 1, 180, 400, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 1, None, 15, 180, 400, 4],
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 1, None, 15, 180, 400, 4],
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 1, None, 1, 180, 400, 0]
        ], columns=['registered_at', 'delta_t', 'speed', 'motor_controller_current_ac', 'motor_controller_voltage_ac', 'motor_controller_angular_speed', 'motor_controller_state'])
        result = add_braking_flags(df, labels=[])

        # When speed is not provided, we can still calculate all other variables
        self.assertEqual(result.shape[0], df.shape[0])
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'generator_power'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'generator_producing_wh'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'generator_producing_energy'].values[0], False)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'generator_power'].values[0], 180 * 15 * 1.5)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'generator_producing_wh'].values[0], 180 * 15 * 1.5 / 3600)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'generator_producing_energy'].values[0], True)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'generator_rpm'].values[0], 0)

if __name__ == '__main__':
    unittest.main()
