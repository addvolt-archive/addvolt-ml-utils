import unittest
import datetime
import pandas as pd
from addvolt_ml_utils.preprocessing.operation import add_low_emission_zone_flags

class AddLowEmissionZoneFlagsTest(unittest.TestCase):
    def setUp(self):
        # Create a simple list of square in a geojson
        self.low_emission_zones = [
            {
                "id": 1,
                "geojson": {
                    "type": "FeatureCollection",
                    "features": [
                        {
                            "type": "Feature",
                            "geometry": {
                                "type": "Polygon",
                                "coordinates": [
                                    [[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]]
                                ]
                            }
                        }
                    ]
                }
            },
            {
                "id": 2,
                "geojson": {
                    "type": "FeatureCollection",
                    "features": [
                        {
                            "type": "Feature",
                            "geometry": {
                                "type": "Polygon",
                                "coordinates": [
                                    [[10, 10], [11, 10], [11, 11], [10, 11], [10, 10]]
                                ]
                            }
                        }
                    ]
                }
            }
        ]

    def tearDown(self):
        pass

    def test_empty_dataframe(self):
        """When passing an empty dataframe, an error should be thrown"""
        with self.assertRaises(Exception):
            df = add_low_emission_zone_flags(pd.DataFrame(), low_emission_zones=self.low_emission_zones)

    def test_empty_df_and_low_emission_zones(self):
        """When passing an empty dataframe and no low emission zones, an error should be thrown"""
        with self.assertRaises(Exception):
            df = add_low_emission_zone_flags(pd.DataFrame(), low_emission_zones=[])

    def test_df_with_missing_location_empty_emission_zones(self):
        """
        When passing an empty low emission zone list even with missing location values, 
        should set in_in_lez flag to false and no lez_id
        """
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0), None, None],
            [datetime.datetime(2017, 1, 1, 10, 1), None, None]
        ], columns=['registered_at', 'latitude', 'longitude'])

        df = add_low_emission_zone_flags(df, low_emission_zones=[])
        self.assertTrue(df['is_in_lez'].value_counts()[False] == df.shape[0])
        self.assertEqual(df.lez_id.isnull().value_counts()[True], df.shape[0])

    def test_df_with_missing_location(self):
        """
        When passing an dataframe with missing location values and no low emission zones, 
        an error should be thrown
        """
        with self.assertRaises(Exception):
            df = pd.DataFrame([
                [datetime.datetime(2017, 1, 1, 10, 0), None, None],
                [datetime.datetime(2017, 1, 1, 10, 1), None, None]
            ], columns=['registered_at', 'latitude', 'longitude'])

            df = add_low_emission_zone_flags(df, low_emission_zones=self.low_emission_zones)

    def test_empty_low_emission_zones(self):
        """When passing an empty low emission zone list, should set in_in_lez flag to false and no lez_id"""
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0), -1, -1],  # Still outside
            [datetime.datetime(2017, 1, 1, 10, 1), 0.5, 0.5],  # Inside first emission zone
            [datetime.datetime(2017, 1, 1, 10, 2), 1, 1],  # On the border
            [datetime.datetime(2017, 1, 1, 10, 3), 2, 2]  # Outside
        ], columns=['registered_at', 'latitude', 'longitude'])

        df = add_low_emission_zone_flags(df, low_emission_zones=[])
        self.assertTrue(df['is_in_lez'].value_counts()[False] == df.shape[0])
        self.assertEqual(df.lez_id.isnull().value_counts()[True], df.shape[0])

    def test_should_trigger_default_on_first_low_emission_zone(self):
        """Should trigger when there's a value inside the boundaries."""
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0), -1, -1],  # Still outside
            [datetime.datetime(2017, 1, 1, 10, 1), 0.5, 0.5],  # Inside first emission zone
            [datetime.datetime(2017, 1, 1, 10, 2), 1, 1],  # On the border
            [datetime.datetime(2017, 1, 1, 10, 3), 2, 2]  # Outside
        ], columns=['registered_at', 'latitude', 'longitude'])

        df = add_low_emission_zone_flags(df, low_emission_zones=self.low_emission_zones)
        self.assertTrue(df['is_in_lez'].value_counts()[True] == 1)
        self.assertEqual(df.lez_id.isnull().value_counts()[True], 3)
        self.assertEqual(len(df[df.lez_id == 1]), 1)

    def test_should_trigger_default_on_second_low_emission_zone(self):
        """Should trigger when there's a value inside the boundaries."""
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0), -10, -10],  # Still outside
            [datetime.datetime(2017, 1, 1, 10, 1), 10.5, 10.5],  # Inside second emission zone
            [datetime.datetime(2017, 1, 1, 10, 2), 11, 11],  # On the border
            [datetime.datetime(2017, 1, 1, 10, 3), 12, 12]  # Outside
        ], columns=['registered_at', 'latitude', 'longitude'])

        df = add_low_emission_zone_flags(df, low_emission_zones=self.low_emission_zones)
        self.assertTrue(df['is_in_lez'].value_counts()[True] == 1)
        self.assertEqual(df.lez_id.isnull().value_counts()[True], 3)
        self.assertEqual(len(df[df.lez_id == 2]), 1)

    def test_should_not_trigger_on_boundaries(self):
        """Should not trigger when the values are on the boundaries but not inside."""
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0), -1, -1],  # Outside
            [datetime.datetime(2017, 1, 1, 10, 1), 0, 0],  # On the first border
            [datetime.datetime(2017, 1, 1, 10, 2), 1, 1],  # On the first border
            [datetime.datetime(2017, 1, 1, 10, 3), 2, 2],  # Outside
            [datetime.datetime(2017, 1, 1, 10, 4), 10, 10],  # On the second border
            [datetime.datetime(2017, 1, 1, 10, 5), 11, 11],  # On the second border
            [datetime.datetime(2017, 1, 1, 10, 6), 12, 12]  # Outside
        ], columns=['registered_at', 'latitude', 'longitude'])

        df = add_low_emission_zone_flags(df, low_emission_zones=self.low_emission_zones)
        self.assertTrue(df['is_in_lez'].value_counts()[False] == 7)
        self.assertEqual(df.lez_id.isnull().value_counts()[True], 7)

if __name__ == '__main__':
    unittest.main()
