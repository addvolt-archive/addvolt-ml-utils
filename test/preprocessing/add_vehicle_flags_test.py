import unittest
import datetime
import pandas as pd
from addvolt_ml_utils.preprocessing.vehicle import add_vehicle_columns


class AddVehicleFlagsTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_is_on_electric_vs_diesel(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 1, 30, 1800],  # Driving in Diesel
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 1, 30, 1800],
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 1, 30, 0],  # Driving in Electric
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 1, 30, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 1, 30, 0]
        ], columns=['registered_at', 'delta_t', 'speed_can', 'rpm'])
        result = add_vehicle_columns(df, rpm_threshold=700, fallback_to_gps=False)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'vehicle_is_on_diesel'].values[0], 1)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'vehicle_is_on_electric'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'vehicle_is_on'].values[0], 1)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'vehicle_is_driving'].values[0], 1)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'vehicle_is_stopped'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'vehicle_is_idle'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'vehicle_is_on_diesel'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'vehicle_is_on_electric'].values[0], 1)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'vehicle_is_on'].values[0], 1)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'vehicle_is_driving'].values[0], 1)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'vehicle_is_stopped'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'vehicle_is_idle'].values[0], 0)

    def test_is_stopped_idle_or_driving(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 1, 0, 0],  # Stopped
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 1, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 1, 0, 1800],  # Idle
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 1, 0, 1800],
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 1, 30, 0],  # Driving
            [datetime.datetime(2017, 1, 1, 10, 0, 5), 1, 30, 0]
        ], columns=['registered_at', 'delta_t', 'speed_can', 'rpm'])
        result = add_vehicle_columns(df, rpm_threshold=700, fallback_to_gps=False)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'vehicle_is_stopped'].values[0], 1)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'vehicle_is_idle'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'vehicle_is_driving'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'vehicle_is_stopped'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'vehicle_is_idle'].values[0], 1)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'vehicle_is_driving'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 4), 'vehicle_is_stopped'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 4), 'vehicle_is_idle'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 4), 'vehicle_is_driving'].values[0], 1)

    def test_fallback_to_gps_is_stopped_or_driving(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 1, 0, 0],  # Stopped
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 1, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 1, 0, 1800],  # Idle but won0t be detected (ignoring RPM when fallback_to_gps is True)
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 1, 0, 1800],
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 1, 30, 0],  # Driving
            [datetime.datetime(2017, 1, 1, 10, 0, 5), 1, 30, 0]
        ], columns=['registered_at', 'delta_t', 'speed', 'rpm'])
        result = add_vehicle_columns(df, rpm_threshold=700, fallback_to_gps=True)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual('vehicle_is_idle' in result, False)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'vehicle_is_stopped'].values[0], 1)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 0), 'vehicle_is_driving'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'vehicle_is_stopped'].values[0], 1)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 2), 'vehicle_is_driving'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 4), 'vehicle_is_stopped'].values[0], 0)
        self.assertEqual(result.loc[result.registered_at == datetime.datetime(2017, 1, 1, 10, 0, 4), 'vehicle_is_driving'].values[0], 1)

if __name__ == '__main__':
    unittest.main()
