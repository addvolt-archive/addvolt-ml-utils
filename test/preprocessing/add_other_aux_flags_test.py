import unittest
import pandas as pd
import numpy as np
import datetime
from addvolt_ml_utils.preprocessing.other_aux import add_other_aux_columns


class AddOtherFlagsTest(unittest.TestCase):
    def setUp(self):
        data = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
        )
        self.df = pd.DataFrame(data=data)
        pass

    def tearDown(self):
        pass

    def test_diesel_always_on(self):
        df = self.df.copy()
        df['timer_digital_2'] = 1
        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_is_on'].value_counts()[True], result.shape[0])  # Every value is True
        self.assertEqual(result['other_aux_diesel_is_on'].value_counts()[True], result.shape[0])  # Every value is True

    def test_diesel_always_off(self):
        df = self.df.copy()
        df['timer_digital_2'] = 0
        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_is_on'].value_counts()[False], result.shape[0])  # Every value is False
        self.assertEqual(result['other_aux_diesel_is_on'].value_counts()[False], result.shape[0])  # Every value is True

    def test_diesel_first_50s_off(self):
        df = self.df.copy()
        df.loc[:50, 'timer_digital_2'] = 0
        df.loc[50:, 'timer_digital_2'] = 1
        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_is_on'].value_counts()[False], 50)
        self.assertEqual(result['other_aux_is_on'].value_counts()[True], 50)
        self.assertEqual(result['other_aux_diesel_startup'].value_counts()[True], 1)  # 1 Startup should be detected

    def test_only_every_30s_a(self):
        df = self.df.copy()
        df.loc[9, 'timer_digital_2'] = 30
        df.loc[39, 'timer_digital_2'] = 30
        df.loc[69, 'timer_digital_2'] = 0

        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_is_on'].value_counts()[True], 40)
        self.assertEqual(result['other_aux_is_on'].value_counts()[False], 60)
        self.assertEqual(result['other_aux_diesel_is_on'].value_counts()[True], 40)
        self.assertEqual(result['other_aux_diesel_is_on'].value_counts()[False], 30)
        self.assertEqual(result['other_aux_diesel_startup'].value_counts()[False], 100)  # No startup detected since the refrigeration unit was already ON

    def test_only_every_30s_b(self):
        df = self.df.copy()
        df.loc[9, 'timer_digital_2'] = 30
        df.loc[39, 'timer_digital_2'] = 30
        df.loc[69, 'timer_digital_2'] = 0
        df.loc[99, 'timer_digital_2'] = 30

        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_is_on'].value_counts()[True], 70)
        self.assertEqual(result['other_aux_is_on'].value_counts()[False], 30)
        self.assertEqual(result['other_aux_diesel_is_on'].value_counts()[True], 70)
        self.assertEqual(result['other_aux_diesel_is_on'].value_counts()[False], 30)
        self.assertEqual(result['other_aux_diesel_startup'].value_counts()[True], 1)  # Startup in second 70
        self.assertEqual(result.loc[70, 'other_aux_diesel_startup'], True)

    def test_electric_mode(self):
        df = self.df.copy()
        df.loc[1:10, 'battery_current'] = -10
        df.loc[1:10, 'load_controller_current_active'] = 10
        df.loc[1:10, 'load_controller_state'] = 2
        df.loc[1:10, 'battery_voltage'] = 300

        df.loc[41:60, 'battery_current'] = -10
        df.loc[41:60, 'load_controller_current_active'] = 10
        df.loc[41:60, 'load_controller_state'] = 2
        df.loc[41:60, 'battery_voltage'] = 300

        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_diesel_is_on'].sum(), 0)
        self.assertEqual(result['other_aux_diesel_startup'].sum(), 0)
        self.assertEqual(result['other_aux_electric_is_on'].sum(), 30)
        self.assertEqual(result['other_aux_is_on'].sum(), 30)

    def test_electric_and_diesel_mode(self):
        df = self.df.copy()
        df.loc[1:10, 'load_controller_current_active'] = 10
        df.loc[1:10, 'load_controller_state'] = 2
        df.loc[1:10, 'battery_voltage'] = 300
        df.loc[41:60, 'load_controller_current_active'] = 10
        df.loc[41:60, 'load_controller_state'] = 2
        df.loc[41:60, 'battery_voltage'] = 300

        df.loc[10, 'timer_digital_2'] = 0
        df.loc[40, 'timer_digital_2'] = 30
        df.loc[99, 'timer_digital_2'] = 0

        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_diesel_is_on'].sum(), 30)
        self.assertEqual(result['other_aux_diesel_startup'].sum(), 1)
        self.assertEqual(result['other_aux_electric_is_on'].sum(), 30)
        self.assertEqual(result['other_aux_is_on'].sum(), 60)

    def test_electric_connected_to_grid(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 300, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 300, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 300, 0, 1],  # Connects to grid
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 300, 0, 1],
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 300, 0, 3],  # Reefer starts from grid
            [datetime.datetime(2017, 1, 1, 10, 0, 5), 300, 0, 3],
            [datetime.datetime(2017, 1, 1, 10, 0, 6), 300, 0, 3],
            [datetime.datetime(2017, 1, 1, 10, 0, 7), 300, 0, 1],  # Reefer stops from grid
        ], columns=['registered_at', 'battery_voltage', 'battery_current', 'grid_state'])

        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_diesel_is_on'].sum(), 0)
        self.assertEqual(result['other_aux_diesel_startup'].sum(), 0)
        self.assertEqual(result['other_aux_electric_is_on'].sum(), 3)
        self.assertEqual(result['other_aux_electric_is_on_from_battery'].sum(), 0)
        self.assertEqual(result['other_aux_electric_is_on_from_grid'].sum(), 3)
        self.assertEqual(result['other_aux_is_on'].sum(), 3)

    def test_electric_from_grid_and_from_battery(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 300, 0, 0, 0],  # Battery_voltage is needed to check if it is working from battery
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 300, 0, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 300, 1, 0, 0],  # Connects to grid
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 300, 1, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 300, 3, 0, 0],  # Reefer starts from grid (Reefer works from here)
            [datetime.datetime(2017, 1, 1, 10, 0, 5), 300, 3, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 6), 300, 3, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 7), 300, 3, -50, 2],  # Reefer working from grid and charging
            [datetime.datetime(2017, 1, 1, 10, 0, 8), 300, 3, -50, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 9), 300, 3, -50, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 10), 300, 3, 0, 0],  # Reefer working from grid but no longer charging
            [datetime.datetime(2017, 1, 1, 10, 0, 11), 300, 3, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 12), 300, 1, 10, 2],  # Reefer switches from working from grid to working from battery (this switch isn't instantaneous in real life)
            [datetime.datetime(2017, 1, 1, 10, 0, 13), 300, 0, 10, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 14), 300, 0, 10, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 15), 300, 1, 0, 0]  # Stops all (Reefer works until here)
        ], columns=['registered_at', 'battery_voltage', 'grid_state', 'load_controller_current_active', 'load_controller_state'])

        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_diesel_is_on'].sum(), 0)
        self.assertEqual(result['other_aux_diesel_startup'].sum(), 0)
        self.assertEqual(result['other_aux_electric_is_on'].sum(), 11)
        self.assertEqual(result['other_aux_electric_is_on_from_battery'].sum(), 3)
        self.assertEqual(result['other_aux_electric_is_on_from_grid'].sum(), 8)
        self.assertEqual(result['other_aux_is_on'].sum(), 11)

    def test_electric_from_grid_and_from_battery_using_grid_available(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 300, 0, 0, 0],  # Battery_voltage is needed to check if it is working from battery
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 300, 0, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 300, 1, 0, 0],  # Connects to grid
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 300, 1, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 300, 3, 0, 0],  # Reefer starts from grid (Reefer works from here)
            [datetime.datetime(2017, 1, 1, 10, 0, 5), 300, 3, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 6), 300, 3, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 7), 300, 3, -50, 2],  # Reefer working from grid and charging
            [datetime.datetime(2017, 1, 1, 10, 0, 8), 300, 3, -50, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 9), 300, 3, -50, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 10), 300, 3, 0, 0],  # Reefer working from grid but no longer charging
            [datetime.datetime(2017, 1, 1, 10, 0, 11), 300, 3, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 12), 300, 1, 10, 2],  # Reefer switches from working from grid to working from battery (this switch isn't instantaneous in real life)
            [datetime.datetime(2017, 1, 1, 10, 0, 13), 300, 0, 10, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 14), 300, 0, 10, 2],
            [datetime.datetime(2017, 1, 1, 10, 0, 15), 300, 1, 0, 0]  # Stops all (Reefer works until here)
        ], columns=['registered_at', 'battery_voltage', 'grid_available', 'load_controller_current_active', 'load_controller_state'])

        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_diesel_is_on'].sum(), 0)
        self.assertEqual(result['other_aux_diesel_startup'].sum(), 0)
        self.assertEqual(result['other_aux_electric_is_on'].sum(), 11)
        self.assertEqual(result['other_aux_electric_is_on_from_battery'].sum(), 3)
        self.assertEqual(result['other_aux_electric_is_on_from_grid'].sum(), 8)
        self.assertEqual(result['other_aux_is_on'].sum(), 11)

    def test_electric_overlapping_wont_overestimate_electric_mode(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 300, 0, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 300, 0, 5, 2],  # Only Battery
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 300, 3, 5, 2],  # Both (In this case it is counted as from battery)
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 300, 3, 0, 0],  # Only Grid
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 300, 0, 0, 0]
        ], columns=['registered_at', 'battery_voltage', 'grid_state', 'load_controller_current_active', 'load_controller_state'])

        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_diesel_is_on'].sum(), 0)
        self.assertEqual(result['other_aux_diesel_startup'].sum(), 0)
        self.assertEqual(result['other_aux_electric_is_on'].sum(), 3)
        self.assertEqual(result['other_aux_electric_is_on_from_battery'].sum(), 2)
        self.assertEqual(result['other_aux_electric_is_on_from_grid'].sum(), 1)
        self.assertEqual(result['other_aux_is_on'].sum(), 3)

    def test_electric_from_grid_bitwise_operation(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 300, 0, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 300, 0, 5, 2],  # Only Battery
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 300, 3, 5, 2],  # Both (In this case it is counted as from battery)
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 300, 6, 0, 0],  # 2nd bit of 6 is 1 so it is feeding from grid
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 300, 10, 0, 0],  # 2nd bit of 10 is 1 so it is feeding from grid
            [datetime.datetime(2017, 1, 1, 10, 0, 5), 300, 0, 0, 0]
        ], columns=['registered_at', 'battery_voltage', 'grid_state', 'load_controller_current_active', 'load_controller_state'])

        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_diesel_is_on'].sum(), 0)
        self.assertEqual(result['other_aux_diesel_startup'].sum(), 0)
        self.assertEqual(result['other_aux_electric_is_on'].sum(), 4)
        self.assertEqual(result['other_aux_electric_is_on_from_battery'].sum(), 2)
        self.assertEqual(result['other_aux_electric_is_on_from_grid'].sum(), 2)
        self.assertEqual(result['other_aux_is_on'].sum(), 4)

    def test_electric_from_grid_bitwise_operation_b(self):
        df = pd.DataFrame([
            [datetime.datetime(2017, 1, 1, 10, 0, 0), 300, 0, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 1), 300, 0, 5, 2],  # Only Battery
            [datetime.datetime(2017, 1, 1, 10, 0, 2), 300, 3, 5, 2],  # Both (In this case it is counted as from battery)
            [datetime.datetime(2017, 1, 1, 10, 0, 3), 300, 6.0, 0, 0],  # 2nd bit of 6 is 1 so it is feeding from grid
            [datetime.datetime(2017, 1, 1, 10, 0, 4), 300, 10, 0, 0],  # 2nd bit of 10 is 1 so it is feeding from grid
            [datetime.datetime(2017, 1, 1, 10, 0, 5), 300, np.nan, 0, 0],
            [datetime.datetime(2017, 1, 1, 10, 0, 6), 300, 0, 0, 0]
        ], columns=['registered_at', 'battery_voltage', 'grid_state', 'load_controller_current_active', 'load_controller_state'])

        result = add_other_aux_columns(df)
        self.assertEqual(len(result), df.shape[0])
        self.assertEqual(result['other_aux_diesel_is_on'].sum(), 0)
        self.assertEqual(result['other_aux_diesel_startup'].sum(), 0)
        self.assertEqual(result['other_aux_electric_is_on'].sum(), 4)
        self.assertEqual(result['other_aux_electric_is_on_from_battery'].sum(), 2)
        self.assertEqual(result['other_aux_electric_is_on_from_grid'].sum(), 2)
        self.assertEqual(result['other_aux_is_on'].sum(), 4)


if __name__ == '__main__':
    unittest.main()
