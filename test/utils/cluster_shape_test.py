import unittest
import pandas as pd
import datetime
from shapely.geometry import LinearRing
from addvolt_ml_utils.utils import cluster_shape


class ClusterShapeTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_return_type_default(self):
        df = pd.DataFrame({
            'latitude': [0, 0, 1, 1],
            'longitude': [0, 1, 1, 0]
        })

        shape = cluster_shape(df.latitude.to_list(), df.longitude.to_list())
        (latitude, longitude) = shape['properties']['centroid']
        self.assertAlmostEqual(latitude, 0.5)
        self.assertAlmostEqual(longitude, 0.5)
        self.assertIsInstance(shape, dict)

    def test_return_shape_type(self):
        df = pd.DataFrame({
            'latitude': [0, 0, 1, 1],
            'longitude': [0, 1, 1, 0]
        })

        shape = cluster_shape(df.latitude.to_list(), df.longitude.to_list(), format='shape')
        self.assertIsInstance(shape, LinearRing)

    def test_shape_no_dilate_cluster_a(self):
        # Cluster A
        # . .
        # . .
        df = pd.DataFrame({
            'latitude': [0, 0, 1, 1],
            'longitude': [0, 1, 1, 0]
        })

        shape = cluster_shape(df.latitude.to_list(), df.longitude.to_list(), dilate=False)
        (latitude, longitude) = shape['properties']['centroid']
        self.assertIsInstance(shape, dict)
        self.assertAlmostEqual(latitude, 0.5)
        self.assertAlmostEqual(longitude, 0.5)
        self.assertTrue(len(shape['geometry']['coordinates'][0]), 5)
    
    def test_shape_no_dilate_cluster_b(self):
        # Cluster B
        #  . 
        # . .
        # . .
        df = pd.DataFrame({
            'latitude': [0, 0, 0.5, 1, 1],
            'longitude': [0, 1, 1.5, 1, 0]
        })

        shape = cluster_shape(df.latitude.to_list(), df.longitude.to_list(), dilate=False)
        (latitude, longitude) = shape['properties']['centroid']
        self.assertIsInstance(shape, dict)
        self.assertAlmostEqual(latitude, 0.5)
        self.assertAlmostEqual(longitude,  0.633, places=3)
        self.assertTrue(len(shape['geometry']['coordinates'][0]), 6)
    
    def test_shape_no_dilate_cluster_c(self):
        # Cluster C
        #  . 
        # ...
        # ...
        df = pd.DataFrame({
            'latitude': [0, 0, 1, 1, 0.5, 0.5, 0.5],
            'longitude': [0, 1, 1, 0, 1.5, 1, 0]
        })

        shape = cluster_shape(df.latitude.to_list(), df.longitude.to_list(), dilate=False)
        (latitude, longitude) = shape['properties']['centroid']
        self.assertIsInstance(shape, dict)
        self.assertAlmostEqual(latitude, 0.5)
        self.assertAlmostEqual(longitude, 0.633, places=3)
        self.assertTrue(len(shape['geometry']['coordinates'][0]), 6)

    def test_shape_centroid(self):
        df = pd.DataFrame({
            'latitude': [0, 0, 2, 2],
            'longitude': [0, 2, 2, 0]
        })

        shape = cluster_shape(df.latitude.to_list(), df.longitude.to_list(), dilate=False)
        (latitude, longitude) = shape['properties']['centroid']
        self.assertIsInstance(shape, dict)
        self.assertAlmostEqual(latitude, 1)
        self.assertAlmostEqual(longitude, 1)
        self.assertTrue(len(shape['geometry']['coordinates'][0]), 5)

if __name__ == '__main__':
    unittest.main()
