import unittest
import pandas as pd
import numpy as np
from addvolt_ml_utils.trip_discretization.grid import fit_into_mgrs


class DetectTripsTest(unittest.TestCase):
    def setUp(self):
        data = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
            latitude=np.arange(40, 41, 0.01),
            longitude=np.arange(-9, -8, 0.01),
            rpm=np.arange(1000, 1100, 1)
        )
        self.df = pd.DataFrame(data=data)

    def tearDown(self):
        pass

    def test_fit_into_mgrs_default(self):
        df = fit_into_mgrs(self.df)
        self.assertTrue('cell_id' in df.columns)
        self.assertFalse(df['cell_id'].isnull().values.any())


if __name__ == '__main__':
    unittest.main()
