import unittest
import pandas as pd
import numpy as np
from addvolt_ml_utils.trip_discretization import detect_trips


class DetectTripsTest(unittest.TestCase):
    def setUp(self):
        data = dict(
            registered_at=pd.date_range('05/05/2017', periods=100, freq='1s'),
            latitude=np.arange(40, 41, 0.01),
            longitude=np.arange(-9, -8, 0.01),
            rpm=np.arange(1000, 1100, 1)
        )
        self.df = pd.DataFrame(data=data)

    def tearDown(self):
        pass

    def test_detect_trips_simple_trip(self):
        df = self.df.copy()

        trips = detect_trips(df)
        columns = trips[0].columns.tolist()

        self.assertTrue('cut' not in columns)
        self.assertTrue(len(trips) == 1)

    def test_detect_trips_2_trips(self):
        df_with_2_trips_separated_by_rpm = self.df.copy()
        df_with_2_trips_separated_by_rpm['rpm'] = 0
        df_with_2_trips_separated_by_rpm.loc[0:49, 'rpm'] = np.arange(1000, 1050, 1)
        df_with_2_trips_separated_by_rpm.loc[51:100, 'rpm'] = np.arange(1000, 1049, 1)

        trips = detect_trips(df_with_2_trips_separated_by_rpm)
        columns = trips[0].columns.tolist()
        self.assertTrue('cut' not in columns)
        self.assertTrue(len(trips) == 2)

    def test_detect_trips_invalid_timestamp(self):
        df_invalid_registered_at = self.df.copy()
        df_invalid_registered_at['registered_at'] = pd.date_range('05/05/2017', periods=100, freq='6min')

        trips = detect_trips(df_invalid_registered_at)
        self.assertTrue(len(trips) == 0)

    def test_detect_trips_custom_timestamp_threshold(self):
        df_invalid_registered_at = self.df.copy()
        df_invalid_registered_at['registered_at'] = pd.date_range('05/05/2017', periods=100, freq='6min')

        # If we set the limit to 7 min the trip should be detected
        trips = detect_trips(df_invalid_registered_at, min_seconds_before_cut=7*60)
        self.assertTrue(len(trips) == 1)


if __name__ == '__main__':
    unittest.main()
