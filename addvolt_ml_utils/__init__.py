"""
Data Utils Library

This library includes main jobs that could be used across different
machine learning jobs.
Specially important to unify standards of preprocessing, column names and flagging.
"""
# Defining pandas display options
import pandas as pd
import addvolt_ml_utils.preprocessing as preprocessing
import addvolt_ml_utils.trip_discretization as trip_discretization
import addvolt_ml_utils.utils as utils

pd.set_option('display.precision', 12)
pd.set_option('display.max_rows', 200)
pd.set_option('display.max_columns', 200)
pd.set_option('display.width', 1000)


