ISLANDING_STATES = [2, 5, 9]


def add_other_aux_columns(df, diesel_field='timer_digital_2'):
    """
    Adds flag columns for any auxiliary system. This is usually used for systems fed by the powerpack that aren't
    refrigeration units. In this situtation, we still want some load related variables, but not associated with
    cold generation itself:

    - other_aux_is_on: Auxiliary system is on or not.
    - other_aux_diesel_is_on: Auxiliary system is in diesel mode or not. This is estimated since we only receive
    a value every 30 seconds.
    - other_aux_diesel_startup: Auxiliary system's diesel model is starting up.
    - other_aux_electric_is_on: Auxiliary system's electric mode is on or not. Based on
    'load_controller_current_active' and 'load_controller_state'.
    - other_aux_electric_is_on_from_grid: Running auxiliary system electric mode from grid.
    - other_aux_electric_is_on_from_battery: Running auxiliary system electric mode from battery.

    :param df: Original dataframe that must have the field passed as parameter and registered_at
    :type df: pd.DataFrame
    :param field: Field in the dataframe with the refrigeration unit timer
    :type field: string
    :return: Dataframe with additional 'other_aux_is_on', 'other_aux_diesel_is_on','other_aux_diesel_startup',
    'other_aux_electric_is_on', 'other_aux_electric_is_on_from_grid' and 'other_aux_electric_is_on_from_battery'
    columns.
    :rtype: pd.DataFrame
    """
    # If the specified field is not found, we will return everything as if the unit never worked.
    if diesel_field not in df:
        print('Field {} not found in the dataframe.'.format(diesel_field))
        df[diesel_field] = 0

    if 'delta_t' not in df:
        df['delta_t'] = (df['registered_at'] - df['registered_at'].shift()).dt.total_seconds()
        df['delta_t'] = df['delta_t'] * (df['delta_t'] < 60) * (df['delta_t'] >= 0)

    # Ensure grid_state column exists.
    if 'grid_state' not in df:
        df['grid_state'] = None

    # Ensure backwards compatibility by supporting default to "grid_available" variable.
    if 'grid_available' in df:
        df['grid_state'] = df.grid_state.fillna(df.grid_available)

    # Diesel
    # Filling rows that explicitly tell us diesel mode fridge is ON or OFF
    df.loc[df[diesel_field] >= 0, 'other_aux_diesel_is_on'] = df[diesel_field] > 0

    # Filling in the rest of the rows wih backward fill
    df['other_aux_diesel_is_on'] = df['other_aux_diesel_is_on'].fillna(method='bfill')
    
    # If we're switching from an OFF state to ON then we found a startup moment.
    df['other_aux_diesel_startup'] = (df['other_aux_diesel_is_on']) & (df['other_aux_diesel_is_on'].shift() == False)  
    
    # Electric
    df['other_aux_electric_is_on'] = False
    df['other_aux_electric_is_on_from_battery'] = False
    df['other_aux_electric_is_on_from_grid'] = False

    # Electric from battery
    if 'load_controller_current_active' in df and 'delta_t' in df:
        df['other_aux_electric_is_on_from_battery'] = (df['load_controller_current_active'] > 1.1) & \
                                                          (df['load_controller_current_active'] < 201) & \
                                                          (df['load_controller_state'].isin(ISLANDING_STATES))

    # Electric from grid is only available if the dataframe has grid_state column
    if 'grid_state' in df and 'delta_t' in df:
        # For detecting this from grid_state, we apply a bitwise mask to fetch the second bit. If it is activated
        # then the system is refrigerating from grid.
        mask = 0b10
        df['other_aux_electric_is_on_from_grid'] = (df.grid_state.fillna(0).astype(int) & mask == 2) & \
                                                       (df['other_aux_electric_is_on_from_battery'] == False)

    df['other_aux_electric_is_on'] = (df['other_aux_electric_is_on_from_battery'] | df['other_aux_electric_is_on_from_grid'])

    # Totals
    df['other_aux_is_on'] = (df['other_aux_diesel_is_on'] | df['other_aux_electric_is_on'])
    return df
