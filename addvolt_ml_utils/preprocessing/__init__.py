"""
Preprocessing Module

This module includes preprocessing tasks that could be used across different
machine learning jobs.
Specially important to unify standards of column names and flagging.
"""
from .data_cleaning import default_data_cleaning
from .data_cleaning import maps_data_cleaning
from .operation import add_operation_flags
