ISLANDING_STATES = [2, 5, 9]


def add_refrigeration_columns(df, field='timer_digital_2'):
    """
    Adds refrigeration flag columns:

    - refrigeration_is_on: If refrigeration unit is on or not. This is estimated since we only receive a value every 30
    seconds.
    - refrigeration_diesel_is_on: Refrigeration unit's diesel mode is on or not. This is estimated since we only receive
    a value every 30 seconds.
    - refrigeration_diesel_startup: Refrigeration unit's diesel model is starting up.
    - refrigeration_diesel_timer: Refrigeration unit's diesel timer. Since we only receive a value every 30 seconds,
    most rows will be empty but the sum will reflect total usage.
    - refrigeration_electric_is_on: Refrigeration unit's electric mode is on or not. Based on
    'load_controller_current_active' and 'load_controller_state'.
    - refrigeration_electric_is_on_from_grid: Running refrigeration unit electric mode from grid.
    - refrigeration_electric_is_on_from_battery: Running refrigeration unit electric mode from battery.
    - refrigeration_electric_timer: Refrigeration unit's electric mode timer. Based on 'load_controller_current_active'
    and 'load_controller_state'.
    - refrigeration_operating_timer: Refrigeration unit's timer sum.
    - refrigeration_operating_timer: Refrigeration unit's timer sum.

    :param df: Original dataframe that must have the field passed as parameter and registered_at
    :type df: pd.DataFrame
    :param field: Field in the dataframe with the refrigeration unit timer
    :type field: string
    :return: Dataframe with additional 'refrigeration_is_on', 'refrigeration_diesel_is_on',
    'refrigeration_diesel_startup', 'refrigeration_diesel_timer', 'refrigeration_electric_is_on' and
    'refrigeration_electric_timer' columns.
    :rtype: pd.DataFrame
    """
    # If the specified field is not found, we will return everything as if the unit never worked.
    if field not in df:
        print('Field {} not found in the dataframe.'.format(field))
        df[field] = 0

    if 'delta_t' not in df:
        df['delta_t'] = (df['registered_at'] - df['registered_at'].shift()).dt.total_seconds()
        df['delta_t'] = df['delta_t'] * (df['delta_t'] < 60) * (df['delta_t'] >= 0)

    # Ensure grid_state column exists.
    if 'grid_state' not in df:
        df['grid_state'] = None

    # Ensure backwards compatibility by supporting default to "grid_available" variable.
    if 'grid_available' in df:
        df['grid_state'] = df.grid_state.fillna(df.grid_available)

    # Diesel
    # Filling rows that explicitly tell us diesel mode fridge is ON or OFF
    df.loc[df[field] >= 0, 'refrigeration_diesel_is_on'] = df[field] > 0

    # Filling in the rest of the rows wih backward fill
    df['refrigeration_diesel_is_on'] = df['refrigeration_diesel_is_on'].fillna(method='bfill')
    df['refrigeration_diesel_timer'] = df[field]
    df['refrigeration_diesel_startup'] = (df['refrigeration_diesel_is_on']) & (df['refrigeration_diesel_is_on'].shift() == False)  # If we're switching from an OFF state to ON then we found a startup moment.

    # Electric
    df['refrigeration_electric_is_on'] = False
    df['refrigeration_electric_is_on_from_battery'] = False
    df['refrigeration_electric_is_on_from_grid'] = False
    df['refrigeration_electric_timer'] = 0

    # Electric from battery
    if 'load_controller_current_active' in df and 'delta_t' in df:
        df['refrigeration_electric_is_on_from_battery'] = (df['load_controller_current_active'] > 1.1) & \
                                                          (df['load_controller_current_active'] < 201) & \
                                                          (df['load_controller_state'].isin(ISLANDING_STATES))

    # Electric from grid is only available if the dataframe has grid_state column
    if 'grid_state' in df and 'delta_t' in df:
        # For detecting this from grid_state, we apply a bitwise mask to fetch the second bit. If it is activated
        # then the system is refrigerating from grid.
        mask = 0b11
        df['refrigeration_electric_is_on_from_grid'] = (df.grid_state.fillna(0).astype(int) & mask == mask) & \
                                                       (df['refrigeration_electric_is_on_from_battery'] == False)
    df['refrigeration_electric_is_on'] = (df['refrigeration_electric_is_on_from_battery'] |
                                          df['refrigeration_electric_is_on_from_grid'])
    df['refrigeration_electric_timer'] = df['refrigeration_electric_is_on'] * df['delta_t']

    # Totals
    df['refrigeration_is_on'] = (df['refrigeration_diesel_is_on'] | df['refrigeration_electric_is_on'])
    df['refrigeration_operating_timer'] = (df['refrigeration_is_on'] * df['delta_t'])
    return df
