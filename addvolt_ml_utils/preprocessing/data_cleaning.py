import pandas as pd
import numpy as np


def default_data_cleaning(df):
    """Cleaning values:

    * Filtering impossible RPM values due to communication errors, if they exist. Replacing them with 0
    * Converting timestamps from text. UTC is assumed
    * Creating additional column with time difference between rows 'delta_t'
    * Creating additional column signaling where and how long connection was lost with 'timer_connection_lost'
    * Flagging GPS errors with new column 'valid_gps'

    Assumes that the following features exist: 'registered_at', 'latitude', 'longitude'

    :param df: Original dataframe containing 'registered_at', 'longitude' and 'latitude' columns
    :type df: pd.DataFrame
    :return: Dataframe with applied cleaning rules.
    :rtype: pd.DataFrame
    """
    if df.empty:
        return pd.DataFrame

    # Filtering out impossible values of rpm. Dont assume column exists because it's not part of base message.
    if 'rpm' in df.columns:
        df['rpm'] = df['rpm'].fillna(0)
        df['rpm'] = df['rpm'] * (df['rpm'] < 5000)

    # Convert registered_at. Assume UTC
    df['registered_at'] = pd.to_datetime(df['registered_at'])
    df['delta_t'] = (df['registered_at'] - df['registered_at'].shift()).dt.total_seconds()

    # Filtering out timers when connection was lost for more than one minute. Datapoint will still usable
    df['timer_connection_lost'] = df['delta_t'] * (df['delta_t'] >= 61) * (df['delta_t'] >= 0)
    df['delta_t'] = df['delta_t'] * (df['delta_t'] < 61) * (df['delta_t'] >= 0)

    # Filtering out with modified z_score
    modified_zscore_latitude = ((df.latitude - df.latitude.median()) / df.latitude.std(ddof=0)).abs()
    modified_zscore_longitude = ((df.longitude - df.longitude.median()) / df.longitude.std(ddof=0)).abs()

    df['valid_gps'] = ((df.latitude - df.shift(1).latitude).fillna(0).abs() < 0.1)

    df.loc[modified_zscore_latitude > 2, 'valid_gps'] = False
    df.loc[modified_zscore_longitude > 2, 'valid_gps'] = False

    return df.copy()


def maps_data_cleaning(df):
    """Cleaning values for maps:
    When we want to draw a map, rows with invalid GPS values must be filtered out, since a single datapoint with bad GPS
    coordinates will spoil the whole map. GPS coordinate errors are also common, since the vehicle is a moving unit.

    * Removes rows where the previous latitude/longitude value is farther than 0.1 decimal points
    * Removes rows where latitude/longitude is outside a square created by the 2nd and 98th percentile of the data

    Assumes that the following features exist: 'latitude', 'longitude'

    :param df: Original dataframe containing 'longitude' and 'latitude' columns
    :type df: pd.DataFrame
    :return: Dataframe with applied cleaning rules.
    :rtype: pd.DataFrame
    """
    if df.empty:
        return pd.DataFrame()

    # Filtering out with modified z_score
    modified_zscore_latitude = ((df.latitude - df.latitude.median()) / df.latitude.std(ddof=0)).abs()
    modified_zscore_longitude = ((df.longitude - df.longitude.median()) / df.longitude.std(ddof=0)).abs()

    df['valid_gps'] = ((df.latitude - df.shift(1).latitude).fillna(0).abs() < 0.1)
    df['valid_gps'] = ((df.longitude - df.shift(1).longitude).fillna(0).abs() < 0.1)

    df.loc[modified_zscore_latitude > 2, 'valid_gps'] = False
    df.loc[modified_zscore_longitude > 2, 'valid_gps'] = False

    return df[df.valid_gps == True].copy()
