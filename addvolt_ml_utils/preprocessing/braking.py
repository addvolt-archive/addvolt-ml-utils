import math

BRAKING_STATES = [4]


def add_braking_flags(df, labels):
    """
    Adds braking flag columns:

    - generator_power 
    - generator_producing_wh 
    - generator_producing_energy
    - generator_rpm

    :param df: Original dataframe with the columns motor_controller_current_ac, motor_controller_voltage_ac, motor_controller_angular_speed and motor_controller_state
    :type df: pd.DataFrame
    :param labels: List of labels to consideren when processing
    :type labels: list
    :return: Dataframe with additional 'generator_power', 'generator_producing_wh', 'generator_producing_energy' and 'generator_rpm' columns.
    :rtype: pd.DataFrame
    """
    if 'delta_t' not in df:
        df['delta_t'] = (df['registered_at'] - df['registered_at'].shift()).dt.total_seconds()
        df['delta_t'] = df['delta_t'] * (df['delta_t'] < 60) * (df['delta_t'] >= 0)

    df['generator_power'] = 0
    if 'motor_controller_current_ac' in df and 'motor_controller_voltage_ac' in df and 'motor_controller_state' in df:
        df['generator_power'] = (1.5 * df['motor_controller_current_ac'] * df['motor_controller_voltage_ac']) * \
                                (df['motor_controller_state'].isin(BRAKING_STATES))

    df['generator_producing_wh'] = (df['generator_power'] * df['delta_t'] / 3600).clip(lower=0)
    df['generator_producing_energy'] = (df['motor_controller_current_ac'] > 2) & (df['motor_controller_current_ac'] < 200)

    df['generator_rpm'] = 0
    if 'motor_controller_angular_speed' in df:
        speed = df.speed_can if 'can' in labels else df.speed
        df['generator_rpm'] = (speed > 15) * (df['motor_controller_angular_speed'] * 60) / (2 * math.pi)

    return df
