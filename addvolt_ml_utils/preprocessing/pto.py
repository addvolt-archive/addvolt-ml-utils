def add_pto_threshold_columns(df, threshold=1000):
    """
    Adds pto flag columns, using a RPM threshold while idled:

    - pto_is_on: If PTO is on or not. This is estimated since we only receive a value every 30 seconds.
    - pto_startup: PTO's diesel model is starting up.
    - pto_operating_timer: PTO's timer sum.

    :param threshold: RPM threshold to consider the PTO working.
    :type threshold: int
    :param df: Original dataframe that must have the 'speed_can' and the 'rpm' column.
    :type df: pd.DataFrame
    :return: Dataframe with additional 'pto_is_on', 'pto_startup' and 'pto_operating_timer' columns.
    :rtype: pd.DataFrame
    """
    # If the specified field is not found, we will return everything as if the unit never worked.
    if 'rpm' not in df or 'speed_can' not in df:
        print('Field {} not found in the dataframe.'.format('rpm'))
        df['rpm'] = 0

    # Check if PTO is operating or not using the threshold. A small filter is added to prevent juggling between
    # ON and OFF in 5 consecutive rows
    df['pto_is_on'] = ((df.rpm >= threshold) & (df.speed_can <= 0))
    df['pto_is_on'] = df['pto_is_on'] | \
                      df['pto_is_on'].shift() | \
                      df['pto_is_on'].shift(2) | \
                      df['pto_is_on'].shift(3) | \
                      df['pto_is_on'].shift(4) | \
                      df['pto_is_on'].shift(5)

    df['pto_operating_timer'] = df['pto_is_on'] * 1
    df['pto_startup'] = (df['pto_is_on']) & (df['pto_is_on'].shift() == False)

    return df
