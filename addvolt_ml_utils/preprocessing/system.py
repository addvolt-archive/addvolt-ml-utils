from .braking import BRAKING_STATES
from .refrigeration import ISLANDING_STATES

CHARGING_STATES = [8, 13]

def add_system_columns(df, labels=[]):
    """
    Adds system flag columns:

    - system_battery_soc: State of Charge of the battery. Received periodically so bfill is applied.
    - system_is_on: System is currently on.
    - system_is_charging: System is charging.
    - system_is_discharging: System is discharging.
    - system_feeding_load_wh
    - system_charging_from_grid_wh
    - system_battery_chg_wh
    - system_battery_dchg_wh

    :param df: Original dataframe.
    :type df: pd.DataFrame
    :param labels: List of labels to consideren when processing
    :type labels: list
    :return: Dataframe with additional 'system_battery_soc', 'system_is_on', 'system_charging' and 'system_discharging' columns.
    :rtype: pd.DataFrame
    """
    if 'delta_t' not in df:
        df['delta_t'] = (df['registered_at'] - df['registered_at'].shift()).dt.total_seconds()
        df['delta_t'] = df['delta_t'] * (df['delta_t'] < 60) * (df['delta_t'] >= 0)
    
    # Ensure grid_state column exists.
    if 'grid_state' not in df:
        df['grid_state'] = None

    # Ensure backwards compatibility by supporting default to "grid_available" variable.
    if 'grid_available' in df:
        df['grid_state'] = df.grid_state.fillna(df.grid_available)

    df['system_battery_soc'] = df['battery_soc'].fillna(method='bfill').fillna(method='ffill')
    df['system_is_charging'] = False
    df['system_is_discharging'] = False
    df['system_grid_available'] = False
    df['system_grid_reversed'] = False
    df['system_remote_control'] = False

    # Calculating battery power to determine if system is charging/discharging
    if 'battery_current' in df:
        df['system_is_charging'] = (df['battery_current'] > 1) & (df['battery_current'] < 200)
        df['system_is_discharging'] = (df['battery_current'] < -2) & (df['battery_current'] > -200)
        df['system_battery_chg_ah'] = 0
        df['system_battery_dchg_ah'] = 0
        # df['system_battery_chg_ah'] = df['battery_current'].apply(lambda c: c if c > 1 and c < 200 else 0) * df['delta_t'] / 3600
        # df['system_battery_dchg_ah'] = df['battery_current'].apply(lambda c: c if c < -2 and c > -200 else 0) * df['delta_t'] / 3600

    if 'grid_state' in df:
        # Apply different bitwise mask to fetch multiple information embed in grid variable.
        _grid_available_mask = 0b1
        _grid_reversed_mask = 0b100
        _remote_mask = 0b1000

        df['system_grid_available'] = (df.grid_state.fillna(0).astype(int) & _grid_available_mask > 0)
        df['system_grid_reversed'] = (df.grid_state.fillna(0).astype(int) & _grid_reversed_mask > 0)
        df['system_remote_control'] = (df.grid_state.fillna(0).astype(int) & _remote_mask > 0) * 1

    # To know how much the battery pack is (dis)charging at a given moment, we also need to know if the generator is producing  
    generator_producing_wh = 0
    if 'regenerative_braking' in labels and 'generator_producing_wh' in df:
        generator_producing_wh = df['generator_producing_wh']
    elif 'regenerative_braking' in labels and \
        'motor_controller_voltage_ac' in df and \
        'motor_controller_current_ac' in df and \
        'motor_controller_state' in df:
        generator_producing_power = (1.5 * df['motor_controller_current_ac'] * df['motor_controller_voltage_ac']) * \
                                (df['motor_controller_state'].isin(BRAKING_STATES))
        generator_producing_wh = generator_producing_power.clip(lower=0) * df['delta_t'] / 3600

    # Calculate the energy being supplied and exiting the AddVolt system (includes braking)
    if 'load_controller_voltage_active' in df and 'load_controller_current_active' in df:
        df['system_feeding_load_power'] = (1.5 * df['load_controller_voltage_active'] * df['load_controller_current_active']) * \
                                    (df['load_controller_state'].isin(ISLANDING_STATES))
        df['system_feeding_load_wh'] = df['system_feeding_load_power'].clip(lower=0) * df['delta_t'] / 3600

        df['system_charging_from_grid_power'] = (-1.5 * df['load_controller_voltage_active'] * df['load_controller_current_active']) * \
                                    (df['load_controller_state'].isin(CHARGING_STATES))
        df['system_charging_from_grid_wh'] = df['system_charging_from_grid_power'].clip(lower=0) * df['delta_t'] / 3600

        # Calculate the energy that is being discharged and charged from the battery at each second
        energy_diff = df['system_feeding_load_wh'] - generator_producing_wh
        df['system_battery_chg_wh'] = (energy_diff * (energy_diff < 0) * -1).clip(lower=0) + df['system_charging_from_grid_wh']
        df['system_battery_dchg_wh'] = (energy_diff * (energy_diff > 0)).clip(lower=0)

    # When powerpack is dc_powered, there are some changes on how we should calculate energy counters.
    # DC-powered systems do not have regenerative braking and feeding load power needs to be calculated from battery current
    # and voltage. This implies recalculation for 'system_battery_chg_wh' and 'system_battery_dchg_wh'. 
    if 'dc_powered' in labels:
        df['system_battery_power'] = df['battery_voltage'] * df['battery_current']
        df['system_feeding_load_power'] = df['system_battery_power'] * df['load_controller_state'].isin(ISLANDING_STATES) * -1
        df['system_feeding_load_wh'] = df['system_feeding_load_power'].clip(lower=0) * df['delta_t'] / 3600
        df['system_charging_from_grid_power'] = df['system_battery_power'] * df['load_controller_state'].isin(CHARGING_STATES)
        df['system_charging_from_grid_wh'] = df['system_charging_from_grid_power'].clip(lower=0) * df['delta_t'] / 3600
        df['system_battery_chg_wh'] = df['system_charging_from_grid_wh']
        df['system_battery_dchg_wh'] = df['system_feeding_load_wh']

    return df
