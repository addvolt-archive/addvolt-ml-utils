def add_photovoltaic_columns(df):
    """
    Adds photovoltaic flag columns:

    - photovoltaic_current: Photovoltaic panels current.
    - photovoltaic_voltage: Photovoltaic voltage.
    - photovoltaic_energy_produced: Photovoltaic energy produced.

    :param df: Original dataframe.
    :type df: pd.DataFrame
    :return: Dataframe with additional 'photovoltaic_current', 'photovoltaic_voltage' and 'photovoltaic_energy_produced'.
    :rtype: pd.DataFrame
    """
    df['photovoltaic_current'] = df['current1']
    df['photovoltaic_voltage'] = df['voltage1']
    df['photovoltaic_energy_produced'] = df['current1'] * df['voltage1'] * -1

    return df
