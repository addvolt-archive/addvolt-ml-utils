from shapely.geometry import Point, shape
import numpy as np
import pandas as pd


def prune_vehicle_status_column(df, delta_t_threshold=120):
    """
    Creates the pruned signal truck status column to add to the original Dataframe df.
    Takes into account false "stopped" status due to traffic or small idling times.
    Input df should include columns "vehicle_status" whose values should be either 'stopped' or 'moving'

    :param df: Original dataframe containing 'registered_at' and 'vehicle_status' column
    :type df: pd.DataFrame
    :param delta_t_threshold: Limit threshold for considering a status change valid (changing from 'moving' to 'stopped' can only occur once every delta_t_threshold seconds).
    :type delta_t_threshold: int
    :return: list with the size of df with pruned signal (list with 'moving' or 'stopped')
    """
    if df.empty:
        return df

    # Store Dataframe indexes where status changes
    status_change_indexes = df[(df.vehicle_status.shift(-1) != df.vehicle_status)].index

    # Store original vehicle_status
    vehicle_status = df.vehicle_status.copy()

    for (index1, index2) in zip(status_change_indexes[0::1], status_change_indexes[1::1]):
        if (df.loc[index2, 'registered_at'] - df.loc[index1, 'registered_at']).total_seconds() < delta_t_threshold:
            vehicle_status.loc[index1:index2] = vehicle_status.loc[index1]

    return vehicle_status


def add_operation_flags(df, rpm_threshold=300, pruning=True):
    """
    Adds vehicle operation flag columns:

    - vehicle_operating: If vehicle is turned on or not
    - vehicle_moving: If Vehicle is moving
    - vehicle_idle: If vehicle is stopped but turned on
    - vehicle_moving_slow: If vehicle is moving below or equal to 40km/h
    - vehicle_moving_fast: If vehicle is moving above 40km/h
    - vehicle_status: Either 'stopped' or 'moving'. This can be pruned by the parameter flag

    :param df: Original dataframe that must have the 'registered_at', 'rpm', and 'speed_can' columns.
    :type df: pd.DataFrame
    :param rpm_threshold: Threshold to consider the vehicle in 'operating' mode
    :type rpm_threshold: int
    :param pruning: Use pruning heuristic to optimize 'vehicle_status' results (avoid falsy 'stopped' status when vehicle is in traffic for example).
    :type pruning: bool
    :return: Dataframe with additional 'vehicle_operating', 'vehicle_moving', 'vehicle_idle', 'vehicle_moving_slow', 'vehicle_moving_fast' and 'vehicle_status' columns.
    :rtype: pd.DataFrame
    """
    # Setting vehicle state flag columns
    df['vehicle_operating'] = df.rpm > rpm_threshold
    df['vehicle_moving'] = (df.speed_can > 0) & (df.vehicle_operating == True)
    df['vehicle_idle'] = (df.speed_can == 0) & (df.vehicle_operating == True)
    df['vehicle_moving_slow'] = (df.vehicle_operating == True) & (df.speed_can <= 40)
    df['vehicle_moving_fast'] = (df.vehicle_operating == True) & (df.speed_can > 40)
    df['vehicle_status'] = np.where((~df['vehicle_operating']) | (df['vehicle_idle']), 'stopped', 'moving')

    # Vehicle status pruning
    if pruning:
        df['vehicle_status'] = prune_vehicle_status_column(df)

    return df


DEFAULT_MIN_SPEED_TO_APPLY_RB = 20


def add_regenerative_braking_flags(df, min_speed_to_apply_rb=DEFAULT_MIN_SPEED_TO_APPLY_RB):
    """
    Identify production points in the vehicle operation. This function checks for the necessary variables to identify
    and count these points, but won't fail if theses colums don't exist.
    This function adds the following columns:

    - braking_point: Boolean identifying if the RB can generate energy in this point (brake_pedal and retarder columns).
    - braking_count: Count of the braking moments detected (brake_pedal and retarder columns).
    - deceleration_point: Boolean identifying if the RB can generate energy in this point (speed_can).
    - deceleration_count: Count of the braking moments detected (speed_can).
    - fuel_rate_point: Boolean identifying if the RB can generate energy in this point (fuel_rate and speed_can).
    - fuel_rate_count: Count of the braking moments detected (fuel_rate and speed_can).

    :param df: Original data to add the columns
    :type df: pd.DataFrame
    :param min_speed_to_apply_rb: Optional argument to identify production points only if the vehicle is operating, at least, at this speed.
    :type min_speed_to_apply_rb: int
    :return: Dataframe with additional columns
    :rtype: pd.DataFrame
    """
    # Identify braking points
    df['braking_point'] = False
    if 'brake_pedal' in df.columns and 'retarder' in df.columns:
        df['braking_point'] = ((df.retarder < 0) | (df.brake_pedal > 1)) & \
                              (df.speed_can > min_speed_to_apply_rb)
    df['braking_count'] = ~(df['braking_point'] & df['braking_point'].shift())
    df['braking_count'] = df['braking_point'] & df['braking_count']

    # Identify deceleration points (vehicle's speed is increasing but no pedal is being pressed)
    df['deceleration_point'] = False
    if 'speed_can' in df.columns and 'speed_can' in df.columns:
        df['delta_speed'] = df.speed_can - df.speed_can.shift()
        df['deceleration_point'] = (df.delta_speed > 1) & \
                                   (~df.braking_point) & \
                                   (df.accelerator_pedal == 0) & \
                                   (df.speed_can > min_speed_to_apply_rb)
    df['deceleration_count'] = ~(df['deceleration_point'] & df['deceleration_point'].shift(-1))
    df['deceleration_count'] = df['deceleration_point'] & df['deceleration_count']

    # Identify where fuel_rate is 0
    df['fuel_rate_point'] = False
    if 'fuel_rate' in df.columns and 'speed_can' in df.columns:
        df['fuel_rate_point'] = (df.fuel_rate == 0) & (df.speed_can > min_speed_to_apply_rb)
    df['fuel_rate_count'] = ~(df['fuel_rate_point'] & df['fuel_rate_point'].shift(-1))
    df['fuel_rate_count'] = df['fuel_rate_point'] & df['fuel_rate_count']

    return df

def add_facilities_flags(df, facilities=None):
    """
    Add new "is_in_facilities" and "entered_facilities" columns that will provide information on whether the system is
    currently inside the facilities provided as parameter. If no facilities are passed, the return dataframe will
    include the columns as always "False".

    'shapely' library is used to create the virtual shapes of the facilities. As such, invalid geojson may still produce
    a shape and it will be used to add facilities flags. Please make sure your geojson is correct, since this function
    was not designed to detect this kind of errors.

    :param df: Original data to add the columns. Must include "latitude" and "longitude" columns
    :type df: pd.DataFrame
    :param facilities: Geojson of the facilities. Con only include one feature.
    :type facilities: dict
    :return: Dataframe with additional column
    :rtype: pd.DataFrame
    """
    if 'latitude' not in df.columns or 'longitude' not in df.columns:
        raise Exception('Missing columns in dataframe.')

    try:
        shapes = []
        for feature in facilities['features']:
            shapes.append(shape(feature['geometry']))
    except (TypeError, KeyError):
        df['is_in_facilities'] = False
        df['entered_facilities'] = False
        return df

    def is_in_facilities(series, shapes):
        current_location = Point(series.longitude, series.latitude)
        for current_shape in shapes:
            if current_shape.contains(current_location):
                return True
        return False

    df['is_in_facilities'] = df.apply(is_in_facilities, shapes=shapes, axis=1)
    df['entered_facilities'] = df['is_in_facilities'] & (df['is_in_facilities'].shift(-1) == False)

    return df

def add_low_emission_zone_flags(df, low_emission_zones=None):
    """
    Add new "is_in_lez" and "lez_id" columns that will provide information on whether the system is
    currently inside a low emission zone from a provided list as parameter. If no low emission zone are passed, 
    the return dataframe will include the columns as always "False".

    'shapely' library is used to create the virtual shapes of a low emission zone. 
    As such, invalid geojson may still produce a shape and it will be used to add low emission zone flags. 
    Please make sure your geojson is correct, since this function was not designed to detect this kind of errors.

    :param df: Original data to add the columns. Must include "latitude" and "longitude" columns
    :type df: pd.DataFrame
    :param lowEmissionZones: list of the low emission zones to check if the vehicle is present.
    :type low_emission_zones: array of dict that must contain and 'id' and 'geojson' fields, for each the emission zone 
    :return: Dataframe with additional column
    :rtype: pd.DataFrame
    """
    if 'latitude' not in df.columns or 'longitude' not in df.columns:
        raise Exception('Missing columns in dataframe.')

    # Provided no low emission zone list, so will never be inside one
    if len(low_emission_zones) == 0:
        df['is_in_lez'] = False
        df['lez_id'] = None
        return df

    try:
        shapes = []
        for low_emission_zone in low_emission_zones:
            for feature in low_emission_zone['geojson']['features']:
                shapes.append({
                    "id": low_emission_zone['id'], 
                    "shape": shape(feature['geometry'])
                    })
    except (TypeError, KeyError):
        df['is_in_lez'] = False
        df['lez_id'] = None
        return df

    def is_in_lez(series, shapes):
        current_location = Point(series.longitude, series.latitude)
        for current_shape in shapes:
            if current_shape["shape"].contains(current_location):
                return pd.Series([ True, current_shape["id"] ])
        return pd.Series([ False, None ])

    in_lez_result = df.apply(is_in_lez, shapes=shapes, axis=1)
    df['is_in_lez'] = in_lez_result[0]
    df['lez_id'] = in_lez_result[1]

    return df
