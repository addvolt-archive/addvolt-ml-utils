def format_duration(seconds):
    """
    Format seconds value into a human readable string.
    Formatted examples:
    '1days(s) 3h 2m 12s'
    '3h 2s'

    :param seconds: Positive integer with the seconds of duration to be formatted to string.
    :type seconds: int
    :return: Formatted string of a time duration
    :rtype: string
    """
    if seconds < 0:
        raise BaseException('Invalid value passed for seconds. Positive value is required')

    days, remainder = divmod(seconds, 60*60*24)
    hours, remainder = divmod(remainder, 60*60)
    minutes, seconds = divmod(remainder, 60)
    return ' '.join([str(int(v)) + l for l, v in zip(('day(s)', 'h', 'm', 's'), (days, hours, minutes, seconds)) if v]) or '0m 0s'
