import json
from shapely.geometry import MultiPoint, Point


def cluster_shape(latitude, longitude, format='dict', dilate=True):
    """Transform cluster datapoints into a convex or concave shape

    :param latitude: List containing latitude elements of a cluster.
    :type latitude: list
    :param longitude: List containing longitude elements of a cluster.
    :type longitude: list
    :param format: Output format: 'dict' returns a GeoJSON format dict; 'shape' returns a shapely.geometry.LinearRing. Defaults to 'dict'.
    :type format: str, optional
    :param dilate: Whether the shape should have a margin around the exterior points or not, defaults to True
    :type dilate: bool, optional
    :return: GeoJSON polygon of the cluster. If 'shape' is specified in 'format', a shapely.geometry.LinearRing is returned instead.
    :rtype: dict
    """
    mp = MultiPoint(list(zip(latitude, longitude)))
    area = mp.convex_hull

    if dilate:
        area = area.buffer(0.00002)

    if format == 'dict':
        return {
            'type': 'Feature',
            'properties': {
                'centroid': list(area.centroid.coords)[0]
            },
            'geometry': {
                'type': 'Polygon',
                'coordinates': [
                    [[p[1], p[0]] for p in area.exterior.coords]
                ]
            }
        }
    
    elif format == 'shape':
        return area.exterior
