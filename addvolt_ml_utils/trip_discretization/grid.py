import numpy as np
import math
import mgrs


def create_grid(max_latitude, min_latitude, max_longitude, min_longitude, step=0.1):
    """
    Creates a grid object with the delimeters passed as parameter.

    The attributes of the returning dict are two main dicts (latitude and longitude) which contain all the data for the
    grid, including all step points, maximum and minimum values

    :param max_latitude: Maximum latitude of the grid.
    :type step: float
    :param min_latitude: Minimum latitude of the grid.
    :type step: float
    :param max_longitude: Maximum longitude of the grid.
    :type step: float
    :param min_longitude: Minimum longitude of the grid.
    :type step: float
    :param step: Size of each cell in Km
    :type step: float
    :return: Object containing all information of a grid
    :rtype: dict
    """
    return dict(
        longitude=dict(
            max=max_longitude,
            min=min_longitude,
            points=grid_pts(
                direction='n',
                step=step,
                min_longitude=min_longitude,
                min_latitude=min_latitude,
                max_longitude=max_longitude,
                max_latitude=max_latitude
            )
        ),
        latitude=dict(
            max=max_latitude,
            min=min_latitude,
            points=grid_pts(
                direction='w',
                step=step,
                min_longitude=min_longitude,
                min_latitude=min_latitude,
                max_longitude=max_longitude,
                max_latitude=max_latitude
            )
        )
    )


def get_bearing(direction):
    """
    Return bearing radian angle for a direction of navigation.
    This is important when iterating around the globe for griding our working area.
    More about bearing in https://en.wikipedia.org/wiki/Bearing_(navigation)

    :param direction: Direction of movement (one of ['north', 'south', 'east', 'west', 'n', 's', 'e', 'w']
    :type direction: string
    :return: Bearing in radian degrees
    :rtype: float
    """
    lower_case_direction = direction.lower()
    if direction == 'n' or lower_case_direction == 'north':
        return np.pi / 2
    elif direction == 's' or lower_case_direction == 'south':
        return np.pi / 2 * - 1
    elif direction == 'e' or lower_case_direction == 'east':
        return 0
    elif direction == 'w' or lower_case_direction == 'west':
        return np.pi
    else:
        raise BaseException('Invalid Bearing direction passed as parameter')


def grid_pts(direction, step, min_longitude, min_latitude, max_longitude, max_latitude):
    """
    Create the grid points for the discretization
    :param direction:
    :param step: step in km
    :param min_longitude:
    :param min_latitude:
    :param max_longitude:
    :param max_latitude:
    :return:
    """
    bearing = get_bearing(direction)
    earth_radius = 6378.137
    result = []

    lon = math.radians(min_longitude)
    lat = math.radians(max_latitude)
    lon_f = math.radians(max_longitude)
    lat_f = math.radians(min_latitude)

    if bearing == get_bearing('north'):
        lat_f = lat_f - 0.001
        result.append(round(math.degrees(lon), 5))
    elif bearing == get_bearing('west'):
        lon_f = lon_f + 0.001
        result.append(round(math.degrees(lat), 5))
    else:
        print('invalid bearing')

    while (lat > lat_f) & (lon < lon_f):
        # Calculating next point with formula given by the report below
        # http://www.movable-type.co.uk/scripts/latlong.html#destPoint
        lat = math.asin(math.sin(lat) * math.cos(step / earth_radius) + math.cos(lat) * math.sin(
            step / earth_radius) * math.cos(bearing))
        lon = lon + math.atan2(math.sin(bearing) * math.sin(step / earth_radius) * math.cos(lat),
                               math.cos(step / earth_radius) - math.sin(lat) * math.sin(lat))

        if bearing == get_bearing('north'):
            result.append(round(math.degrees(lon), 5))
        elif bearing == get_bearing('west'):
            result.append(round(math.degrees(lat), 5))
        else:
            print('invalid bearing')
    return result


def fit_into_grid(df, grid):
    """
    Fits GPS coordinates into the grid given by latitude/longitude points adding a cell_id, row_nbr and col_nbr column to the Dataframe
    :param df: Original dataframe with latitude and longitude columns
    :param grid: Grid dictionary
    :return: Dataframe with additional 'cell_id', 'row_nbr', col_nbr' column.
    """
    latitude_points = grid['latitude']['points']
    longitude_points = grid['longitude']['points']

    for lt in latitude_points:
        df.ix[(df.latitude < lt), 'row_nbr'] = latitude_points.index(lt) + 1

    for lg in reversed(longitude_points):
        df.ix[(df.longitude < lg), 'col_nbr'] = longitude_points.index(lg)

    df['cell_id'] = (df['row_nbr'] - 1) * (len(longitude_points) - 1) + df['col_nbr']
    return df


m = mgrs.MGRS()


def fit_into_mgrs(df, column='cell_code'):
    df[column] = df.apply(lambda r: m.toMGRS(r['latitude'], r['longitude'], MGRSPrecision=3).decode('utf-8'), axis=1)
    return df


def mgrs_to_gps(code):
    return m.toLatLon(code)
