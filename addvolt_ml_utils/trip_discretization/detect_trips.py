import pandas as pd
import numpy as np
from datetime import timedelta

# Algorithm parameters
RPM_THRESHOLD = 300
SECONDS_BEFORE_NEXT_ENTRY = 5 * 60
MIN_ENTRIES_IN_TRIP = 10  # 10
MIN_DISTANCE_IN_TRIP = 5  # 5


def detect_trips(df, min_seconds_before_cut=SECONDS_BEFORE_NEXT_ENTRY):
    """
    TODO: Explain the process

    :param min_seconds_before_cut:
    :param df: DatFrame with 'registered_at', 'rpm', 'latitude', 'longitude' and 'speed_can' columns to detect trips from
    :type df: pd.DataFrame
    :return: List of pd.DataFrame, each containing all information of a trip
    :rtype: list
    """
    trips = []
    timedelta_before_next_entry = timedelta(seconds=min_seconds_before_cut)

    # Flagging where cuts should happen
    df['cut'] = 0

    # Cutting when time threshold met
    if df.registered_at.dtypes is not np.datetime64:
        df['registered_at'] = pd.to_datetime(df['registered_at'])

    df['cut'] = np.where(df['registered_at'] - df['registered_at'].shift(1) > timedelta_before_next_entry, 1, df['cut'])
    df['cut'] = np.where(abs(df['latitude'] - df['latitude'].shift(1)) > 0.1, 1, df['cut'])
    df['cut'] = np.where(abs(df['longitude'] - df['longitude'].shift(1)) > 0.1, 1, df['cut'])

    # Cutting when RPM threshold met
    df['cut'] = np.where(df.rpm < RPM_THRESHOLD, 1, df['cut'])

    # Listing where cuts should happen, including initial and end points of whole dataset.
    cut_indexes = list(df.loc[df.cut == 1].index)
    cut_indexes.insert(0, 0)
    cut_indexes.append(df.shape[0])

    del df['cut']

    for begin, end in zip(cut_indexes, cut_indexes[1:]):
        if begin + 1 == end:
            continue
        trips.append(df.loc[begin + 1:end - 1])

    # Remove trips with less than X entries
    trips = [x for x in trips if len(x) > MIN_ENTRIES_IN_TRIP]

    return trips
